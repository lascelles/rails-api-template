FROM ruby:2.6
RUN apt-get update -yqq && apt-get install -yqq --no-install-recommends

RUN mkdir /budgeting
WORKDIR /budgeting

ENV BUNDLE_PATH /gems

COPY Gemfile* /budgeting/

RUN bundle install
COPY . /budgeting

# Add a script to be executed every time the container starts.
COPY scripts/entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]

CMD ["bin/rails", "s", "-b", "0.0.0.0"]
